// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TDS/TDS.h"
#include "TDS/Interface/TDS_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTDS_StateEffect> AddEffectclass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectclass)
	{
		
		UTDS_StateEffect* myEffect = Cast<UTDS_StateEffect>(AddEffectclass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStackable)
					{
						int8 j = 0;
						TArray<UTDS_StateEffect*> CurrentEffect;
						ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffect = myInterface->GetAllCurrentEffects();
						}
						if (CurrentEffect.Num()>0)
						{
							while (j < CurrentEffect.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffect[j]->GetClass() != AddEffectclass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{
						UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(TakeEffectActor, AddEffectclass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}
					}
						
				}
				i++;
			}
		}

	}
}