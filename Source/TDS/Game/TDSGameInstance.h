// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDS/FunctionLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "TDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//table
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "WeaponSettings")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "WeaponSettings")
		UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName ItemName, FDropItem& OutInfo);
};
